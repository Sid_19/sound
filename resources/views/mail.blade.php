<html>
	<head>
		
		<style>
			body {
				margin: 0 !important;
				padding: 0 !important;
				-webkit-text-size-adjust: 100% !important;
				-ms-text-size-adjust: 100% !important;
				-webkit-font-smoothing: antialiased !important; 	/*style only recognised by some browsers*/
			}
			tr {
    text-align: center;
}
			img {
				border: 0 !important;
				outline: none !important;
			}

			p {
				Margin: 0px !important; /*Old versions of Outlook ignore margin if it is lower case as usual*/
				padding: 0px !important;
			}

			table {
				border-collapse: collapse;
				mso-table-lspace: 0px;	/*Microsoft Office only styling*/
				mso-table-rspace: 0px;	/*Microsoft Office only styling*/
			}

			td, a, span {
				border-collapse: collapse;
				mso-line-height-rule: exactly;	 /*Microsoft Office only styling*/
			}

			.ExternalClass * {
				line-height: 100%;
			}

			@media yahoo { 
			  	.em_img {
			  		min-width:700px !important;
			  	} 
			}

			.em_img {
				width: 400px !important;
    height: auto !important;
    padding: 18px 0;
			}

			/* Text decoration removed */
			.em_defaultlink a {
				color: inherit !important;
				text-decoration: none !important;
			}

			span.MsoHyperlink {
				mso-style-priority: 99; 	/*Microsoft Office only styling*/
				color: inherit;
			}

			span.MsoHyperlinkFollowed {
				mso-style-priority: 99; 	/*Microsoft Office only styling*/
				color: inherit;
			}

			/* Media Query for desktop layout */
			@media only screen and (min-width:481px) and (max-width:699px) {
				.em_main_table {
					width: 100% !important;
				}
				.em_wrapper {
					width: 100% !important;
				}
				.em_hide {
					display: none !important;
				}
				.em_img {
					width: 100% !important;
					height: auto !important;
				}
				.em_h20 {
					height: 20px !important;
				}
				.em_padd {
					padding: 20px 10px !important;
				}
			}

			/* Media Query for mobile layout */
			@media screen and (max-width: 480px) {
				.em_main_table {
					width: 100% !important;
				}
				.em_wrapper {
					width: 100% !important;
				}
				.em_hide {
					display: none !important;
				}
				.em_img {
					width: 100% !important;
					height: auto !important;
				}
				.em_h20 {
					height: 20px !important;
				}
				.em_padd {
					padding: 20px 10px !important;
				}
				.em_text1 {
					font-size: 16px !important;
					line-height: 24px !important;
				}
				u + .em_body .em_full_wrap {
					width: 100% !important;
					width: 100vw !important;
				}
			}
</style>

		</style>
	</head>
	<body class="em_body" style="margin:0px; padding:0px;" bgcolor="#efefef">
	 
		<table class="em_full_wrap" valign="top" style="width:700px;background-color: #f6f7f8;" cellspacing="0" cellpadding="0" border="0" bgcolor="#efefef" align="center" bgcolor="#f6f7f8">
  			<tbody>
  				<tr>
    				<td valign="top" align="center">
    					<table class="em_main_table" style="width:700px;" width="700" cellspacing="0" cellpadding="0" border="0" align="center">
					        <!--Header section-->
					        <tbody>
					        	
					        <!--//Header section ends--> 
                           
        					<!--Banner section-->
       							<tr>
          							<td valign="top" align="center">
          								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
              								<tbody>
              									<tr>
                									<td valign="top" align="center">
                										<img class="em_img" alt="Welcome to EmailWeb Newsletter" style="display:block; font-family:Arial, sans-serif; font-size:30px; line-height:34px; color:#000000; " src="<?= url("/") ?>/storage/u-1/2021/06/16/new-project-2021-06-03t153305579-1622714607-1623822507.png" height="120px" class="logo-blur"  style="width: 30%!important;">
                									</td>
              									</tr>
            								</tbody>
            							</table>
            						</td>
        						</tr>
        						 <tr>
                               <td colspan="2">
                                   
                                      <p style="text-align: center;"> Hi Admin, </br>
	    <?php  $user = get_user_by_id(get_current_user_id()); ?>
                             {{ $user->first_name}} {{ $user->last_name}} sent you new approval request mentioned below:-</p>
                               </td> 
                                
                            </tr>
        						<tr>
          							<td valign="top" align="center">
          								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="  padding: 43px">
              								<tbody>
        <tr>
            <td>
                
               
            </td>
            <td></td>
        </tr>      									
  <tr>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><strong>Document Type:</strong></td>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><?= $maildata->document_type ?></td>
  </tr>
  
   <tr>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><strong>Document Number :</strong></td>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><?= $maildata->document_no ?></td>
  </tr>
  <tr>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><strong>Front Image:</strong></td>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><img src="<?= url("/") ?>/storage/documents/<?= $maildata->front_file ?>" width="400px"/></td>
  </tr>
    <tr>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><strong>Rear Image:</strong></td>
    <td style="font-family: Arial; font-size:12px; color:#4d251b; padding: 7px;"><img src="<?= url("/") ?>/storage/documents/<?= $maildata->back_file ?>" width="400px"/></td>
  </tr>
            								</tbody>
            							</table>
            						</td>
        						</tr>
        					<!--//Banner section ends--> 
        					<!--Content Text Section-->
        				
        					<!--//Content Text Section Ends--> 
                    
        					<!--Footer Section. Change the FB, Twitter, LinkedIn links for your own, and it is best to use your own images that you host for the social media buttons so you control the links to the images, also then you can use different or alternate images if you don't like these. Jpegs and PNG work best-->
        						<tr>
          							<td style="padding:38px 30px;" class="em_padd" valign="top" bgcolor="#f6f7f8" align="center">
          								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
              								<tbody>
              									<tr>
                									<td style="padding-bottom:16px;" valign="top" align="center">
                										<table cellspacing="0" cellpadding="0" border="0" align="center">
                    										<tbody>
                    										
                  											</tbody>
                  										</table>
                  									</td>
              									</tr>
              									<tr>
                									<td style="font-family:'Open Sans', Arial, sans-serif; font-size:11px; line-height:18px; color:#999999;" valign="top" align="center">
                										 
                										
                										
                  											<?= date('Y') ?> � All Right Reserved. Developed & Hosted by. soundboxstudios.com.au
                  										<br>
                  										
                  									</td>
              									</tr>
            								</tbody>
            							</table>
            						</td>
        						</tr>
      						</tbody>
      					</table>
      				</td>
  				</tr>
			</tbody>
		</table>

		<!-- extra spacing to force the desktop layout in Gmail -->
		<div class="em_hide" style="white-space: nowrap; display: none; font-size:0px; line-height:0px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>

	</body>

</html>