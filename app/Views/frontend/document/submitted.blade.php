@include('frontend.components.header')
<div class="sr-root">
      <div class="sr-main text-center" style="padding:50px 0px;">
        <section class="container">
          <div>
          
            <h1>Your ID has been successfully submitted.</h1>
            <h3 id="waiting-msg">Please check back in 5 minutes.</h3>
            <button class="btn btn-primary" type="button" id="loader" disabled>
              <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              Your profile is under process...
            </button>
            <!--<p id="myDiv">Your current status is :<?= $current_status ?>  </p>-->
            <div id="errmsg"></div>
            <div id="verify-again" style="font-size:15px;"></div>
        </section>
      </div>
    </div>
    <script type="text/javascript">
    (function(){
    jQuery("#error-message").hide();
    })
    function greet()
    {
       // var myDiv = document.getElementById("myDiv");
        //myDiv.style.display = "none";
        $.ajax({
                type: "GET",
                url: "{{ url('/sendstatus') }}",
                data: "jjhj",
                success: function (msg) {
                    if(msg == "verified")
                    {
                        console.log("now verified");
                        $("#waiting-msg").hide();
                        $("#loader").hide();
                        // console.log('{{ url('/sendmail') }}');
                        // exit;
                       // window.location.href = 'sendmail';
                       window.location.href = '{{ url('/sendmail') }}';
                       
                        
                        
                    }
                    else
                    {
                       // console.log("not verified");
                       $("#waiting-msg").hide();
                       $("#loader").hide();
                         $("#errmsg").html("<h3>Your ID wasn't accepted. Please try again or get in touch by clicking the messenger icon below, or giving us a call on 0431 736 798.");
                         var url = "{{url('/verification')}}";
                         console.log(url);
                         $("#verify-again").html('<a href="' + url + '">Verify Again</a>');
                        
                        
                    }
                    
                   //console.log(msg);
                   
                }
            
            });
        
           
        
  
       
     
    }
  setTimeout(greet, 60000);
    

   </script>

    @include('frontend.components.footer')