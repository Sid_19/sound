@include('frontend.components.header')


  <div class="verify text-center" style="background-color: #f3f3f3; padding: 50px 0px;">
  <h2>You still need to verify your account before you can make a booking </h2>
  <button id="verify-button" class="btn btn-primary text-uppercase book-btn">Verify Now</button>
  </div>
  <div class="submit text-center" style="padding: 50px 10px;">
  <h2>Why do you need to submit your ID?</h2>
  <p>We have designed Soundbox Studios to be as accessible to everyone as possible. To make our space available to everyone 24/7 whilst still ensuring our equipment remains safe, we need to know exactly who is using the studios.
  </p>
  <p>Verifying your account is super simple and will take about 2 minutes to submit and 15 minutes until you can make a booking.</p>
  </div>
    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', async () => {

        // Set your publishable key: remember to change this to your live publishable key in production
        // Find your keys here: https://dashboard.stripe.com/apikeys
        const stripe = Stripe('<?= $publish_key ?>');
        console.log(stripe);
        var verifyButton = document.getElementById('verify-button');
        verifyButton.addEventListener('click', async () => {
          // Get the VerificationSession client secret using the server-side
          // endpoint you created in step 3.

          try {

            // Create the VerificationSession on the server.
            //const response = await fetch('create-verification-session.php', {
            //  method: 'POST',
           // })
      
      //const response = await fetch('create-verification-session.php');
      const data = <?= $data ?>;
      //console.log(data);
      const client_secret = data.client_secret;
      

      const id = data.id;
      console.log('@clint'+client_secret+"@id"+id);
            // Open the modal on the client.
            const {error} = await stripe.verifyIdentity(client_secret);
            if(!error) {//console.log(response);
              window.location.href = 'submitted/'+id;
            } else {
              alert(error.message);
            }
          } catch(e) {
            alert(e.message);
          }
        })
      })
    </script>
 
 @include('frontend.components.footer')