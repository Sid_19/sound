@include('frontend.components.header')
<div class="sr-root">
      <div class="sr-main">
        <section class="container">
          <div class="text-center">
          
            <h1>Your account has been verified. You are now free to make a booking 24/7 via our website.</h1>
            <a href="<?=url('/')?>/home-search-result" style="font-size:15px;" class="menu-bookbtn">Make A Booking</a>
           
          </div>
        </section>
      </div>
    </div>
</script>

@include('frontend.components.footer')