@include('frontend.components.header')
<?php
enqueue_style('mapbox-gl-css');
enqueue_style('mapbox-gl-geocoder-css');
enqueue_script('mapbox-gl-js');
enqueue_script('mapbox-gl-geocoder-js');

$map_lat = get_option('contact_map_lat');
$map_lng = get_option('contact_map_lng');
$contact_detail = get_option('contact_detail');
?>
@if($map_lat && $map_lng)

<!-- Start of soundboxstudios Zendesk Widget script -->
<!-- End of soundboxstudios Zendesk Widget script -->
<style>
    @media (min-width: 1600px)
    {
.container {
    max-width: 1150px;
}
}
.page-content-inner iframe {
    margin-top: 40px;
}
.info-box p {
    font-size: 20px;
    color: #000;
}
.info-box {
    text-align: center;
}
.info-box.mt-4 {
    text-align: center;
}
.info-box {
    border: 2px solid#e6dfdc;
    margin-top: 35px;
    padding-bottom: 11px;
    box-shadow: 11px 12px 0px 0px #e6dfdc;
}
#footer .subscribe-form button {
    height: 50px;
    border: none;
    background: #e15928!important;
    color: var(--white);
    position: relative;
    float: right;
    right: 0;
    padding: 0 30px;
    font-size: 21px;
    border-radius: 0 .2rem .2rem 0;
}
.info-box.data-chane ul li {
    font-size: 21px;
    list-style: none;
    text-align: left;
}
.change-btn {
    background-color: #e15928;
    color: white;
    border: 2px solid#f1556c;
    padding: 65px;
    font-size: 18px;
}
.change-btn:hover {
    color: #fff;
}
.pagesits {
    text-align: left;
    display: inline-block;
    padding: 80px 0 0 13px;
}

 @media (max-width: 992px)
 {
    .pagesits 
    {
        text-align: left;
        display: inline-block;
        padding: 0 0 0 0;
        
    }
 }
 .info-box.data-chane ul li a {
    color: black;
}
</style>
<div class="page-archive pb-4">
    <div class="banner" style="background: #eee url('http://soundboxstudios.com.au/webdev/storage/u-1/2021/05/27/imgpsh-fullsize-anim-8-1622111311.jpg') center center/cover no-repeat;"></div>
    <div class="container">
        <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="http://soundboxstudios.com.au/webdev">Homepage</a>
                </li>
                <li class="breadcrumb-item">Contact Us</li>
            </ol>
            </nav>
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="page-content">
                    <!--<h1 class="title">About us</h1>-->
                    <div class="page-content-inner">
                         <div class="container ">
                             
                             <!--iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3566.6516235896224!2d152.9576751150939!3d-26.627611287786983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9378fc5031e157%3A0xc897067838d82b0c!2s131%20Currie%20St%2C%20Nambour%20QLD%204560%2C%20Australia!5e0!3m2!1sen!2sin!4v1624535736102!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe-->
                                
            <!-- row end -->

            
            
            <!-- row end -->
        </div>                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="contact-us-detail">
                    <form action="<?= url('/')?>/contact-us-post" class="form-sm mt-3 form-action form-contact-us" method="post" data-validation-id="form-contact-us" data-google-captcha="yes">
                        <h3 class="mb-3">Stay in touch with us</h3>
                      {{ csrf_field() }}    
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input id="contact_email" type="text" class="form-control has-validation" data-validation="required|email" name="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <input id="contact_title" type="text" class="form-control has-validation" data-validation="required" name="title" placeholder="Title">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="contact_message" class="form-control" placeholder="Your Message"></textarea>
                        </div>
                        <input type="submit" class="btn btn-primary" name="submit" value="Send Message">
                        <!--<a href="https://soundboxstudios.zendesk.com/hc/en-us" class="float-right btn help-des ">Help Desk</a>-->
                          
                        <div class="form-message"></div>
                    </form>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="pagesits">
                    <a href="https://soundbox-studios.zammad.com/help/en-gb" target="_blank" class="float-right btn change-btn ">User Base<br> (Soundbox Tips & Tricks)</a>
                </div>
            </div>
        <div class="col-lg-4 col-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="info-box data-chane"> 
                        <h3>Our Address</h3> 
                        <ul>
                            <li><strong>Postal address</strong></li>
                            <li>1B/131 Currie Street, Nambour, 4560, QLD</li>
                        </ul>
                        <ul>
                            <li><strong>Accessed via: </strong>36 Howard Street, Nambour, 4560, QLD</li>
                            
                        </ul>
                        <!--<p class="address">-->
                        <!--    131 currie street, Nambour,4560, QLD<br>-->
                        <!--    Accessed via: 36 Howard street, Nambour, 4560, QLD</p>-->
                        <h3>Email Us</h3>
                        <ul>
                           
                            <li><a href="mailto:hello@soundboxstudios.com.au">hello@soundboxstudios.com<br>.au</a></li>
                        </ul>
                        
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="col-12 col-lg-3">-->
            <!--    <div class="page-sidebar">-->
            <!--        <div class="widget-item category">
    <h3 class="widget-title">Categories</h3>
    <div class="widget-content">
                <a href="http://soundboxstudios.com.au/webdev/category/travel">Travel</a>
                <a href="http://soundboxstudios.com.au/webdev/category/museums">Museums</a>
                <a href="http://soundboxstudios.com.au/webdev/category/mountains">Mountains</a>
                <a href="http://soundboxstudios.com.au/webdev/category/cultural-events">Cultural events</a>
                <a href="http://soundboxstudios.com.au/webdev/category/beauty-beaches">Beauty beaches</a>
            </div>
</div>

<div class="widget-item post-recent">
    <h3 class="widget-title">Recent posts</h3>
    <div class="widget-content">
                <div class="post-item">
            <div class="thumbnail-wrapper">
                <a href="http://soundboxstudios.com.au/webdev/post/23/a-seaside-reset-in-laguna-beach">
                                        <img src="https://dummyimage.com/100x100/e0e0e0/c7c7c7.png"
                         alt="A Seaside Reset in Laguna Beach"
                         class="img-fluid"/>
                                    </a>
            </div>
            <div class="content">
                <h3 class="title">
                    <a href="http://soundboxstudios.com.au/webdev/post/23/a-seaside-reset-in-laguna-beach">A Seaside Reset in Laguna Beach</a>
                </h3>
                <p class="date">Jan 03, 2020</p>
            </div>
        </div>
                <div class="post-item">
            <div class="thumbnail-wrapper">
                <a href="http://soundboxstudios.com.au/webdev/post/22/city-spotlight-philadelphia">
                                        <img src="https://dummyimage.com/100x100/e0e0e0/c7c7c7.png"
                         alt="City Spotlight: Philadelphia"
                         class="img-fluid"/>
                                    </a>
            </div>
            <div class="content">
                <h3 class="title">
                    <a href="http://soundboxstudios.com.au/webdev/post/22/city-spotlight-philadelphia">City Spotlight: Philadelphia</a>
                </h3>
                <p class="date">Jan 03, 2020</p>
            </div>
        </div>
                <div class="post-item">
            <div class="thumbnail-wrapper">
                <a href="http://soundboxstudios.com.au/webdev/post/21/all-aboard-the-rocky-mountaineer">
                                        <img src="http://soundboxstudios.com.au/webdev/storage/u-1/2021/06/16/new-project-2021-06-16t115325368-1623824643-100x100.png"
                         alt="All Aboard the Rocky Mountaineer"
                         class="img-fluid"/>
                                    </a>
            </div>
            <div class="content">
                <h3 class="title">
                    <a href="http://soundboxstudios.com.au/webdev/post/21/all-aboard-the-rocky-mountaineer">All Aboard the Rocky Mountaineer</a>
                </h3>
                <p class="date">Jan 03, 2020</p>
            </div>
        </div>
                <div class="post-item">
            <div class="thumbnail-wrapper">
                <a href="http://soundboxstudios.com.au/webdev/post/20/a-seaside-reset-in-laguna-beach">
                                        <img src="http://soundboxstudios.com.au/webdev/storage/u-1/2021/06/16/new-project-2021-06-16t120431899-1623825306-100x100.png"
                         alt="A Seaside Reset in Laguna Beach"
                         class="img-fluid"/>
                                    </a>
            </div>
            <div class="content">
                <h3 class="title">
                    <a href="http://soundboxstudios.com.au/webdev/post/20/a-seaside-reset-in-laguna-beach">A Seaside Reset in Laguna Beach</a>
                </h3>
                <p class="date">Jan 03, 2020</p>
            </div>
        </div>
                <div class="post-item">
            <div class="thumbnail-wrapper">
                <a href="http://soundboxstudios.com.au/webdev/post/19/my-12-favorite-cities-in-the-world">
                                        <img src="http://soundboxstudios.com.au/webdev/storage/u-1/2021/06/03/new-project-2021-06-03t152711437-1622714291-100x100.png"
                         alt="My 12 Favorite Cities in the World"
                         class="img-fluid"/>
                                    </a>
            </div>
            <div class="content">
                <h3 class="title">
                    <a href="http://soundboxstudios.com.au/webdev/post/19/my-12-favorite-cities-in-the-world">My 12 Favorite Cities in the World</a>
                </h3>
                <p class="date">Jan 03, 2020</p>
            </div>
        </div>
            </div>
</div>
-->
            <!--    </div>-->
            <!--</div>-->
        </div>
    </div>
</div>

    
@endif
@include('frontend.components.footer')
