<?php
$logo_footer = get_option('footer_logo');
if (empty($logo_footer)) {
    $logo_footer = get_option('logo');
}
$list_social = get_option('list_social');
$screen = current_screen();
$setup_mailc_api = get_option('mailchimp_api_key');
$setup_mailc_list_id = get_option('mailchimp_list');
enqueue_script('nice-select-js');
enqueue_style('nice-select-css');
?>
</div>
<footer id="footer" class="{{ $screen == 'home-search-result' ? 'hide-footer' : '' }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                @if(!empty($logo_footer))
                    <img src="{{ get_attachment_url($logo_footer) }}" alt="footer logo" class="footer-logo"/>
                @endif
                @if(!empty($list_social))
                    <ul class="social">
                        @foreach($list_social as $item)
                            <li>
                                <a href="{{ $item['social_link'] }}">
                                    {!! get_icon($item['social_icon']) !!}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
           <div class="col-lg-3 col-md-12">
                        <h4 class="footer-title">{{ get_option('footer_menu1_label') }}</h4>
                        <?php
                        $menu_id = get_option('footer_menu1');
                        get_nav_by_id($menu_id);
                        ?>
            </div>
            <div class="col-lg-3 col-md-12">
                        <h4 class="footer-title">Studios</h4>
                        <ul class="menu">	
                        
                				<li class="menu-item menu-item-2a ">
                			<a href="http://soundboxstudios.com.au/webdev/home-search-result?home-type=98">Band Studios</a>
                					</li>
                					<li class="menu-item menu-item-1 ">
                			<a href="http://soundboxstudios.com.au/webdev/home-search-result?home-type=99">DJ Studios</a>
                					</li>
                			
                		</ul>
            </div>
            <div class="col-lg-3 col-md-12">
                        <!--<h4 class="footer-title">{{ get_option('footer_menu2_label') }}</h4>-->
                        <a href="http://soundboxstudios.com.au/webdev/page/5/about-us"><h5 class="footer-title">Where's Soundbox?</h5></a>
                        
            </div>
                    
                
           
        </div>
        <div class="copy-right d-flex align-items-center justify-content-between">
            <div class="clearfix">
                {!! balanceTags(get_option('copy_right')) !!}
            </div>
        </div>
    </div>
</footer>
</div>
<?php do_action('footer'); ?>
<?php do_action('init_footer'); ?>
<?php do_action('init_frontend_footer'); ?>
<script src="{{asset('js/frontend.js')}}"></script>

<!-- Messenger Chat Plugin Code -->
    <div id="fb-root"></div>
    <!-- Your Chat Plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "105600745096950");
      chatbox.setAttribute("attribution", "biz_inbox");
    </script>
    <!-- Your SDK code -->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v13.0'
        });
      };
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_GB/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
</body>
</html>
