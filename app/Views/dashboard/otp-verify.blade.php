@include('dashboard.components.header')
<script>
 <?php if(!get_user_meta($user_id,'is_otp_verified')): ?>
            fbq('track', 'CompleteRegistration');  
            console.log("coming here to hit event");
 <?php endif;?>
</script>
<?php     
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}

?>
<style>





.card h6 {
    color: #6F1667;
    font-size: 20px
}
.inputs input {
    width: 40px;
    height: 40px;
     padding-right: 12px !important;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
   
}

.card-2 {
    background-color: #fff;
    padding: 10px;
    width: 415px;
    height: 100px;
    bottom: -50px;
    left: 20px;
    position: absolute;
    border-radius: 5px
}

.card-2 .content {
    margin-top: 50px
}

.card-2 .content a {
    color: red
}

.form-control:focus {
    box-shadow: none;
    border: 2px solid red
}

.validate {
    border-radius: 20px;
    height: 40px;
    background-color: red;
    border: 1px solid red;
    width: 140px
}
.span {
    margin: 10px;
}
</style>
<div class="account-pages hh-dashboard mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card bg-pattern">
                    <div class="card-body p-4">
                        <div class="text-center w-75 m-auto">
                            <a class="logo" href="{{ dashboard_url() }}">
                                <?php
                                $logo = get_option('dashboard_logo');
                                $logo_url = get_attachment_url($logo);
                                ?>
                                <img src="{{ $logo_url }}" alt="{{get_attachment_alt($logo)}}">
                            </a>
                            <p class="text-muted  mt-3">{{__("Please enter the one time password
to verify your account.")}}</p>
<p class="text-muted  mt-3">{{__("A code has been sent to .")}} {{custom_echo($usermobile->mobile,4)}}</p>
                        </div>
                        <form id="hh-reset-password-form" action="{{ url('auth/otp-verify') }}" method="post"
                              data-reload-time="1500"
                              data-validation-id="form-otp-verify"
                              class="form form-action">
                            @include('common.loading')
                            <div class="form-group inputs  d-flex flex-row justify-content-center mt-2"  id="otp">
                              
                                <!--<input class="m-2 text-center form-control rounded" name="otp1" type="text" id="first" maxlength="1" /> -->
                                <!--<input class="m-2 text-center form-control rounded" name="otp2" type="text" id="second" maxlength="1" />-->
                                <!--<input class="m-2 text-center form-control rounded" name="otp3" type="text" id="third" maxlength="1" />-->
                                <!--<input class="m-2 text-center form-control rounded" name="otp4" type="text" id="fourth" maxlength="1" />-->
                                
                                <input class="m-2 text-center form-control rounded" name="otp1" id="codeBox1" type="number" maxlength="1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)" />
                                <input class="m-2 text-center form-control rounded" name="otp2" id="codeBox2" type="number" maxlength="1" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)" />
                                <input class="m-2 text-center form-control rounded" name="otp3" id="codeBox3" type="number" maxlength="1" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)" />
                                <input class="m-2 text-center form-control rounded" name="otp4" id="codeBox4" type="number" maxlength="1" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)" />
                                
                                <input type="hidden" name="user" value="<?= $user_id  ?>"/>
                            </div>
                           <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary btn-block text-uppercase"
                                        type="submit"> {{__('Validate')}} </button>
                            </div>
                              <div class="card-2">
            <div class="content d-flex justify-content-center align-items-center"> 
                
          
            <span class="span">Didn't get the code? </span>
               <span class="span"> <a href="javascript:void(0)" class="text-decoration-none ms-3"  onclick="resentotp()">  Resend OTP</a></span> </div>
        </div>
                            <div class="form-message">

                            </div>
                        </form>

                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

               
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>


<script>
 function getCodeBoxElement(index) {
            return document.getElementById('codeBox' + index);
        }
function onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if (getCodeBoxElement(index).value.length === 1) {
        if (index !== 4) {
            getCodeBoxElement(index + 1).focus();
        } else {
            getCodeBoxElement(index).blur();
        }
    }
    if (eventCode === 8 && index !== 1) {
        getCodeBoxElement(index - 1).focus();
    }
}
function onFocusEvent(index) {
    for (item = 1; item < index; item++) {
        const currentElement = getCodeBoxElement(item);
        if (!currentElement.value) {
            currentElement.focus();
            break;
        }
    }
}   
</script>



<script>
document.addEventListener("DOMContentLoaded", function(event) {

// function OTPInput() {
// const inputs = document.querySelectorAll('#otp > *[id]');
// for (let i = 0; i < inputs.length; i++) { inputs[i].addEventListener('keydown', function(event) { if (event.key==="Backspace" ) { inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); } else { if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } else if (event.keyCode> 64 && event.keyCode < 91   ) { inputs[i].value=String.fromCharCode(event.keyCode); if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } } }); } } OTPInput(); });

function resentotp()
{
    jQuery('.hh-loading ').show();
          jQuery.ajax({
                       type:'get',
                       url:'{{ url('auth/resent-otp') }}/{{$user_id}}',
                       success:function(response)
                       {
                             jQuery('.hh-loading ').hide();
                           jQuery('.form-message').html(response.message);
                          

                       }


          })

}
</script>


@include('dashboard.components.footer')
