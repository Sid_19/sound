@include('dashboard.components.header')
<?php
$user = get_current_user_data();
?>
<script>
    
    window.location.href='<?= url('/') ?>/verification';
    
</script>
<div id="wrapper">
    @include('dashboard.components.top-bar')
    @include('dashboard.components.nav')
    <div class="content-page">
        <div class="content">
            {{--Start Content--}}
            <div class="page-title-box">
                <h4 class="page-title">{{__('Verification Document Upload ')}}
                    !</h4>
            </div>
            
            <div class="card card-box">
                 
                    <div class="col-12 col-md-8 offset-md-2">
                <div class="payment-item">
                    <div class="payment-status">
                  
                    </div>
                @if(\Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif
                
                 @if(\Session::has('error'))
                <p class="alert  alert-danger">{{ Session::get('success') }}</p>
                @endif
                    
                
                 @if(!empty($document_verify) and isset($document_verify->is_verified) )
                    @if(!empty($document_verify->admin_comment))
                      <p class="alert  alert-warning">{{$document_verify->admin_comment}}</p>
                      
                   @elseif($document_verify->is_verified)
                        <p class="alert  alert-success">Your documents are verified successfully.Feel Free to contact us if any query.</p>
                   @else
                     <p  style="text-align:center" class="alert  alert-warning">
                          
                          You document verification is under progress
                          
                       
                      </p>
                    <p style="text-align:center"> <img class="img-fluid" src="<?= url('/') ?>/images/svg/working-progress-svgrepo-com.png" width="600"></p>
                    
                       
                   @endif
                 @else
                 
                    <div class="payment-detail">
                        
                        
                       <form  action="{{ url('/verification/submit') }}" method="post" enctype="multipart/form-data"
      >
        <div class="form-group">
            <label for="checkinout">{{ __('Document type') }} <span style="color:red">*</span></label>
            <div class="date-wrapper date-double">
                 @csrf
                  <select class="form-control" onchange="selecttime(this)"  required  name="document_type" id="document_type">
                        <option value=""> Select document Type</option>
                        <option value="Passport">Passport</option>
                         <option value="Driving License">Driving License</option>
                         <option value="Medicare Card">Medicare Card</option>
                     </select>
            </div>
            
            
            </div>
            
         <div class="form-group">
            <label for="checkinout">{{ __('Document Number') }} <span style="color:red">*</span></label>
            <div class="date-wrapper date-double">
                 <input type="text" name="document_no" required class="form-control"  placeholder="Document Number" /> 
              
            </div>
            
            
            </div>    
          <div class="form-group">
            <label for="checkinout">{{ __('Front Image') }} <span style="color:red">*</span></label>
            <div class="date-wrapper date-double">
                 <input type="file" name="front_file"  required class="form-control" placeholder="
                 Document Number" /> 
              
            </div>
            
            
            </div>    
             <div class="form-group">
            <label for="checkinout">{{ __('Back Image') }} <span style="color:red">*</span></label>
            <div class="date-wrapper date-double">
                 <input type="file" name="back_file" required class="form-control"  placeholder="
                 Document Number" /> 
              
            </div>
            
            
            </div>    
            
            
             <div class="form-group">
                  
               <div class="text-center mt-5">
                       <button
                           class="btn btn-primary ">{{__('Submit')}}</button>
                    </div>      
            
            
            </div>
            
            </form>
                    </div>
                  @endif
                   
                </div>
            </div>
                
            </div> 
         
        </div>
        {{--End content--}}
        @include('dashboard.components.footer-content')
    </div>
</div>
<div class="modal fade hh-get-modal-content" id="modal-show-booking-invoice" tabindex="-1" role="dialog"
     aria-hidden="true" data-url="{{ dashboard_url('get-booking-invoice') }}">
    <div class="modal-dialog">
        <div class="modal-content">
            @include('common.loading')
            <div class="modal-header">
                <h4 class="modal-title">{{__('Booking Detail')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>
@include('dashboard.components.footer')
