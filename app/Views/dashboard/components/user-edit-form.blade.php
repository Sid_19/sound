<?php
enqueue_script('nice-select-js');
enqueue_style('nice-select-css');

enqueue_script('flatpickr-js');
enqueue_style('flatpickr-css');
$user_role = get_user_role($user->getUserId(), 'id');
?>
<div class="form-group">
    <label for="user_email">{{__('Email')}}</label>
    <div class="form-control">
        {{ $user->email }}
    </div>
</div>
<div class="row">
    <div class="col-12 col-sm-6">
        <div class="form-group">
            <label for="user_first_name">{{__('First Name')}} <span class="f12">{{__('(Optional)')}}</span></label>
            <input type="text" class="form-control" id="user_first_name_edit" name="user_first_name"
                   value="{{ $user->first_name }}">
        </div>
    </div>
    <div class="col-12 col-sm-6">
        <div class="form-group">
            <label for="user_last_name">{{__('Last Name')}} <span class="f12">{{__('(Optional)')}}</span></label>
            <input type="text" class="form-control" id="user_last_name_edit" name="user_last_name"
                   value="{{ $user->last_name }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-sm-12">
        <div class="form-group">
            <label for="user_role">{{__('Role')}}</label>
            <select name="user_role" id="user_role_edit" class="form-control wide" data-plugin="customselect">
                <option value="">{{__('---- Select ----')}}</option>
                <?php
                $roles = get_all_roles();
                ?>
                @foreach($roles as $key => $role)
                    <option @if($user_role == $key) selected @endif value="{{ $key }}">{{ $role }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
 <div class="col-6 col-md-12">
                                    <div class="form-group">
                                        <label for="mobile">{{__('Mobile')}}</label>
                                        <input type="text" class="form-control" id="mobile" name="mobile"
                                               value="{{ $user->mobile }}">
                                    </div>
                                </div>

        <div class="col-6 col-md-12">
                                <div class="form-group">
                            <label for="password-reg-form">{{__('What is your Band/Artist name')}}?</label>
                            <?php   $band_or_artist_name = get_user_meta($user->id,'band_or_artist_name'); ?>
                            <input class="form-control has-validation" data-validation="required|band_or_artist_name"
                                   name="band_or_artist_name" value="{{$band_or_artist_name}}" type="text" id="band_or_artist_name"
                                   placeholder="{{__('What is your Band/Artist name')}}?">
                        </div></div>
                        <div class="col-6 col-md-12">
                            <div class="form-group">
                            <label for="mobile-address-reg-form">{{__('Your preffered Studio')}}</label>
                              <?php   $preffered_studio = get_user_meta($user->id,'preffered_studio'); ?>
                             <select name="preffered_studio" id="preffered_studio" 
                              
                             class="form-control has-validation" data-validation="required|preffered_studio">
                             <option value="" >Select Type</option>
                             <option value="DJ" <?= $preffered_studio=='DJ'?'selected="selected"':''?>>DJ</option>
                             <option value="Rehearsal" <?= $preffered_studio=='Rehearsal'?'selected="selected"':''?>>Rehearsal</option>
                             </select>
                        </div>   
                        </div>
                        <?php $verified_user ?>
                        @if($verified_user != null)
                         <div class="col-6 col-md-12">
                           <div class="form-group">
                           <label for="mobile-address-reg-form">{{__('User Status')}}</label>
                            <select name="user_status" id="user_status"
                             <?php $is_verified ?>
                            class="form-control has-validation" data-validation="required|user_status">
                            <option value="1" <?= $is_verified=='1'?'selected="selected"':''?>>Verified</option>
                            <option value="0" <?= $is_verified=='0'?'selected="selected"':''?>>Unverified</option>
                            </select>
                       </div>  
                       </div> 
                         @else
                         <div class="col-6 col-md-12">
                           <div class="form-group">
                           <label for="mobile-address-reg-form">{{__('User Status')}}</label>
                            <select name="user_status" id="user_status"
                            class="form-control has-validation" data-validation="required|user_status">
                            <option disabled selected value> -- Select status -- </option>
                            <option value="1"> Verified </option>
                            <option value="0"> Unverified </option>
                            </select>
                       </div>  
                       </div> 
                         @endif
                        
</div>                           
<div class="form-group">
    <label for="user_password">{{__('Password')}}</label>
    <div class="input-group">
        <input type="text" class="form-control" data-plugin="password" id="user_password_edit" name="user_password">
        <div class="input-group-append">
            <button class="btn btn-dark waves-effect waves-light" type="button">{{__('Create Password')}}</button>
        </div>
    </div>
</div>
<input type="hidden" name="userID" value="{{ $user->getUserId() }}">
<input type="hidden" name="userEncrypt" value="{{ hh_encrypt($user->getUserId()) }}">
