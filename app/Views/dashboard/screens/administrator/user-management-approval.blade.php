@include('dashboard.components.header')
<?php
enqueue_script('confirm-js');
enqueue_style('confirm-css');

?>
<style>
.card-box .header-area form {
    position: relative;
    width: 28rem;
    margin-left: auto;
}
.formelement{
    float: left;
    margin-left: 10px;

}
</style>    
<div id="wrapper">
    @include('dashboard.components.top-bar')
    @include('dashboard.components.nav')
    <div class="content-page">
        <div class="content">
            @include('dashboard.components.breadcrumb', ['heading' => __('User Document Approval Request  Management')])
            
              @if(\Session::has('message'))
<p class="alert alert-success">{{ \Session::get('message') }}</p>
@endif

 @if(\Session::has('error'))
<p class="alert alert-danger">{{ \Session::get('error') }}</p>
@endif
            {{--Start Content--}}
            <div class="card-box">
                <div class="header-area d-flex align-items-center">
                    
                  
                    <h4 class="header-title mb-0">{{__('All Users')}}</h4>
                    <form class="form-inline right d-none d-sm-block" method="get" id="seachdoc">
                         <div class="form-group formelement">
                          <select name="status" class="form-control wide " data-plugin="customselect" onchange="statusUpdate(this)">
                               <option value="" >Search by Status </option>
                              
                           
                             <option value="1" <?= (isset($_GET['status'])&& $_GET['status']==1?'selected':'') ?> >Approved</option>
                              <option value="0"  <?= (isset($_GET['status'])&& $_GET['status']=='0'?'selected':'') ?> >Approval Pending</option>
                                <option value="2" <?= (isset($_GET['status'])&& $_GET['status']==2?'selected':'') ?> >Rejected</option>
                           </select>
                           </div>
                        <div class="form-group">
                            <?php
                            $search = request()->get('_s');
                            $order = request()->get('order', 'desc');
                            ?>
                            <input type="text" class="form-control" name="_s"
                                   value="{{ $search }}"
                                   placeholder="{{__('Search by id, email')}}">
                        </div>
                        <button type="submit" class="btn btn-default"><i class="ti-search"></i></button>
                    </form>
                </div>
                <?php
                enqueue_style('datatables-css');
                enqueue_script('datatables-js');
                enqueue_script('pdfmake-js');
                enqueue_script('vfs-fonts-js');
                ?>
                <?php
                $tableColumns = [0, 1, 2, 3, 4,5,6,7];
                ?>
                <table class="table table-large mb-0 dt-responsive nowrap w-100" data-plugin="datatable"
                       data-paging="false"
                       data-export="on"
                       data-pdf-name="{{__('Export to PDF')}}"
                       data-csv-name="{{__('Export to CSV')}}"
                       data-cols="{{ base64_encode(json_encode($tableColumns)) }}"
                       data-ordering="false">
                    <thead>
                    <tr>
                        <?php
                        $_order = ($order == 'asc') ? 'desc' : 'asc';
                        $url = add_query_arg([
                            'orderby' => 'id',
                            'order' => $_order
                        ]);
                        ?>
                        <th data-priority="0">
                            <a href="{{ $url }}" class="order">
                                {{__('ID')}}
                                @if($order == 'asc')
                                    <i class="icon-arrow-down"></i>
                                @else
                                    <i class="icon-arrow-up"></i>
                                @endif
                                <span class="exp d-none">{{__('ID')}}</span>
                            </a>
                        </th>
                        <th data-priority="1">
                            {{__('Name')}}
                        </th>
                          <th data-priority="1">{{__('Email')}}</th>
                          <th data-priority="2">{{__('Phone Number')}}</th>
                        <th data-priority="3">{{__('Document Type')}}</th>
                          <th data-priority="4">{{__('Document No')}}</th>
                        <th data-priority="7" >
                            
                                
                                    {{__('Document Front')}}
                                 
                              
                          
                        </th>
                         <th data-priority="8" >
                            
                                
                                    {{__('Document Back')}}
                                 
                              
                          
                        </th>
                       <th data-priority="5" class="text-center">{{__('Status')}}</th>
                        <th data-priority="6" class="text-center">{{__('Actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($allUsers['total'])
                        @foreach ($allUsers['results'] as $item)
                            <?php
                            $userID = $item->id;
                            ?>
                            <tr>
                                <td class="align-middle">
                                    #{{ $userID }}
                                </td>
                                <td class="align-middle">
                                  
                                        {{ $item->first_name }} {{ $item->last_name }}
                                  
                                      
                                   
                                </td>
                                <td class="align-middle">  {{ $item->email }}</td>
                                <td class="align-middle">  {{ $item->mobile }}</td>
                                <td class="align-middle">
                                    {{ $item->document_type }}
                                </td>
                                <td class="align-middle">
                                    <span class="role-status exp {{ $item->role_slug }}">{{ $item->document_no }} </span>
                                </td>
                                <td class="align-middle text-center">
                                   <a href="<?= url("/") ?>/storage/documents/{{ $item->front_file }}" download><img src="<?= url("/") ?>/storage/documents/{{ $item->front_file }}" width="200px"/></a>
                                </td>
                                 <td class="align-middle text-center">
                                  
                                      <a href="<?= url("/") ?>/storage/documents/{{ $item->back_file }}" download> <img src="<?= url("/") ?>/storage/documents/{{ $item->back_file }}" width="200px"/></a>
                                </td>
                                <td>
                                 @if($item->is_verified==2)
                                           <span class="badge badge-danger">Rejected </span>
                                    @elseif($item->is_verified==0)
                                           <span class="badge badge-warning">Pending </span>
                                    @else
                                          <span class="badge badge-success">Verified </span>
                                    @endif  
                                </td>
                                <td class="align-middle text-center">
                                   
                                    
                                    <div class="dropdown dropleft">
                                        <a href="javascript: void(0)" class="dropdown-toggle table-action-link"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                                class="ti-settings"></i></a>
                                        <div class="dropdown-menu">
                                            <?php
                                            $params = [
                                                'userID' => $userID,
                                                'userEncrypt' => hh_encrypt($userID)
                                            ];
                                            ?>
                                             @if(!$item->is_verified)
                                                    <a href="<?= url("/") ?>/dashboard/studentdocumentapprove/{{$userID}}" class="dropdown-item">{{__('Approve')}}</a>

                                                    @if($item->role_slug != 'customer')
                                                        <a href="javascript:void(0)" class="dropdown-item text-danger"
                                                        data-toggle="modal"
                                                        data-params="{{ base64_encode(json_encode($params)) }}"
                                                        data-target="#hh-delete-user-modal">{{__('Reject')}}</a>
                                                    @else
                                                        <a href="javascript:void(0)" class="dropdown-item text-danger"
                                                        data-toggle="modal"
                                                        onclick="adduserid(<?= $userID ?>)"
                                                        data-target="#hh-delete-user-modal">{{__('Reject')}}</a>
                                                    @endif
                                               @endif

                                                <?php
                                            $params = [
                                                'userID' => $userID,
                                                'userEncrypt' => hh_encrypt($userID)
                                            ];
                                            ?>
                                            <a href="javascript:void(0)" class="dropdown-item hh-link-action hh-link-change-status-home text-danger" 
                                                      data-action="<?= url("/") ?>/dashboard/studentdocumentdelete/{{$userID}}"
                                                   data-confirm="yes"
                                                   data-confirm-title="{{__('System Alert')}}"
                                                   data-confirm-question="{{__('Are you sure to delete this document?')}}"
                                                   data-confirm-button="{{__('Delete it!')}}"
                                                   data-parent="tr"
                                                   data-is-delete="true"
                                                     data-params="{{ base64_encode(json_encode($params)) }}"
                                                     
                                                       >{{__('Delete')}}</a>   
                                        </div>
                                    </div>
                                 
                                   
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">
                                <h4 class="mt-3 text-center">{{__('No users yet.')}}</h4>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="clearfix mt-2">
                    {{ dashboard_pagination(['total' => $allUsers['total']]) }}
                </div>
            </div>
            <div id="hh-update-user-modal" class="modal fade hh-get-modal-content" tabindex="-1" role="dialog"
                 aria-hidden="true"
                 data-url="{{ dashboard_url('get-user-item') }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form class="form form-action form-update-coupon-modal relative"
                              data-validation-id="form-update-user"
                              action="{{ dashboard_url('update-user-item') }}">
                            @include('common.loading')
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('Update User')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                </button>
                            </div>
                            <div class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button type="submit"
                                        class="btn btn-info waves-effect waves-light">{{__('Update')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->

            <div id="hh-delete-user-modal" class="modal fade " tabindex="-1" role="dialog"
                 aria-hidden="true"
                >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form class="form form-action form-update-coupon-modal relative"
                              data-validation-id="form-delete-user"
                              action="{{ dashboard_url('studentdocumentreject') }}">
                            @include('common.loading')
                            <div class="modal-header">
                                <h4 class="modal-title">{{__(' Document Reject Reason')}}</h4>
                              
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                </button>
                            </div>
                            <div class="modal-body">  <textarea name="admin_comment" placeholder="Enter Rejection Reason" style="width:100%"></textarea>
                            <input type="hidden" id="user_id" name="user_id" />
                            </div>
                            <div class="modal-footer">
                                <button type="submit"
                                        class="btn btn-info waves-effect waves-light">{{__('Submit')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->
            
            <div id="hh-delete-user-modal-confirm" class="modal fade hh-get-modal-content" tabindex="-1" role="dialog"
                 aria-hidden="true"
                >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form class="form form-action form-update-coupon-modal relative"
                              data-validation-id="form-delete-user"
                              id="deleterecord"
                              action="">
                           
                            <div class="modal-header">
                                <h4 class="modal-title">{{__('Delete Document')}}</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                </button>
                            </div>
                            <div class="modal-body">  
                               Are you sure to delete this document?
                            </div>
                            <div class="modal-footer">
                                <button type="submit"
                                        class="btn btn-info waves-effect waves-light">{{__('Confirm Deletion')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->
            <script>
                function adduserid(userid)
                {
                    jQuery('#user_id').val(userid);
                    
                }
            </script>
            {{--End content--}}
            @include('dashboard.components.footer-content')
        </div>
    </div>
</div>
<script>

function statusUpdate(event)
{
    if(jQuery(event).val() !='')
    {
     jQuery('#seachdoc').submit(); 
    }
}

function deleteit(event)
{  
    jQuery('#deleterecord').attr('action',jQuery(event).attr('data-url'));
}
</script>
@include('dashboard.components.footer')
