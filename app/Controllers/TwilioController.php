<?php

    namespace App\Controllers;

    use App\Http\Controllers\Controller;
    use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
    use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;
    use Mockery\Exception;
    use Redirect;
    use Sentinel;
    use Twilio\Rest\Client;


    class TwilioController extends Controller
    {
       protected $sid = 'ACfb8fddf39f584e3b1d0cf14f433e5f3e';
       protected $token = '51b1e20509255a7405efa434893cba23';
       protected $client;
      
          function __construct()
         {
            $this->client = new Client($this->sid, $this->token);


         }
          
/******** Generate OTP */
         public function generateNumericOTP($n) {
      
            // Take a generator string which consist of
            // all numeric digits
            $generator = "1357902468";
          
            // Iterate for n-times and pick a single character
            // from generator and append it to $result
              
            // Login for generating a random character from generator
            //     ---generate a random number
            //     ---take modulus of same with length of generator (say i)
            //     ---append the character at place (i) from generator to result
          
            $result = "";
          
            for ($i = 1; $i <= $n; $i++) {
                $result .= substr($generator, (rand()%(strlen($generator))), 1);
            }
          
            // Return result
            return $result;
        }
          public function sendOTP($data)
          {
             $otp = $this->generateNumericOTP(4);
             $this->client->messages->create(
                // the number you'd like to send the message to
                     //'+61'.$data['mobile'],
                     $data['mobile'],
                [
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+13213207287',
                    // the body of the text message you'd like to send
                    'body' => 'Your soundbox account verification OTP is :'.$otp
                ]
            );
            $data['otp'] =$otp;
            $data['otp_expirary'] = (time()+60);   
            if(\DB::table('user_otp')->where(['user_id'=>$data['user_id']])->get()->count()>0):
            return \DB::table('user_otp')->where(['user_id'=>$data['user_id']])->update($data);
            else:
                return \DB::table('user_otp')->insert($data);  
            endif;
            



          }

    }