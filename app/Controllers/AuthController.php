<?php

    namespace App\Controllers;

    use App\Http\Controllers\Controller;
    use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
    use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;
    use Mockery\Exception;
    use Redirect;
    use Sentinel;
    use Mail;

    class AuthController extends Controller
    {
        public function subscribeEmail()
        {
            $email = request()->get('email');
            $res = \MailChimpSubscribe::get_inst()->addNewSubscriber($email);
            $this->sendJson($res, true);
        }

        public function _getSignUp()
        {
            return view('dashboard.sign-up', ['bodyClass' => 'authentication-bg authentication-bg-pattern']);
        }

        public function _postSignUp(Request $request)
        {
            $validator = Validator::make($request->all(),
                [
                    'email' => 'required',
                    'mobile' => 'required|unique:users',
                    'password' => 'required|min:6',
                    'preffered_studio' => 'required',
                    'band_or_artist_name' => 'required',
                    'term_condition' => 'required'
                ],   
                [
                    'email.required' => __('The email is required'),
                    'mobile.required' => __('The mobile is required'),
                    'password.required' => __('The password is required'),
                    'preffered_studio.required' => __('Select preffered studio is required'),
                    'band_or_artist_name.required' => __('Brand and Artist name is required'),
                    'password.min' => __('The password has at least 6 characters'),
                    'term_condition.required' => __('Please agree with the Term and Condition')
                ]  
            );
            if ($validator->fails()) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => $validator->errors()->first()])->render()
                ]);
            }
            $password = request()->get('password');
            $hear_about_us = request()->get('hear_about_us');
            
            $credentials = [
                'email' => request()->get('email'),
                'password' => $password,
                'hear_about_us' => $hear_about_us,
                'first_name' => request()->get('first_name', ''),
                'last_name' => request()->get('last_name', ''),
                'mobile' =>request()->get('mobile','')  
            ];

            $new_user = create_new_user($credentials);
            try
            {
           
            if(!$new_user['status']){
                  
                 
               
              
                  return $this->sendJson([
                    'status' => 0,  
                    'message' => view('common.alert', ['type' => 'danger', 'message' => $new_user['message']])->render()
                ]);
            }else{

                   $sendotp = new \App\Controllers\TwilioController();
                 $sendotp->sendOTP(['user_id'=>$new_user['user']->getUserId(),'mobile'=>request()->get('mobile','')]);  
                return $this->sendJson([
                    'status' => 1,
                    'message' => view('common.alert', ['type' => 'success', 'message' => $new_user['message']])->render(),
                    'redirect' => url('auth/otp-verify/'.$new_user['user']->getUserId())
                ]);
            }

             }catch(\Twilio\Exceptions\RestException $ex)
             {

                  return $this->sendJson([
                    'status' => 0,  
                    'message' => view('common.alert', ['type' => 'danger', 'message' => $ex->getMessage()])->render()
                ]);

             }
        }
 /********** Resend OTP ****/
        public function _getResendOTP($id)
        {

              $usercred = \DB::table('user_otp')->where(['user_id'=>$id])->get()->first();   
              $sendotp = new \App\Controllers\TwilioController();


              if(empty($usercred)):
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => 'No OTP Generated yet'])->render()
                ]);
            endif;
              $sendotp->sendOTP(['user_id'=>$id,'mobile'=> $usercred->mobile]);  

                 return $this->sendJson([
                        'status' => 1,
                        'message' => view('common.alert', ['type' => 'success', 'message' => __('Sent New OTP on your registered Mobile Number')])->render(),
                    ]); 
        }

        public function _getResetPassword()
        {
            return view('dashboard.reset-password', ['bodyClass' => 'authentication-bg authentication-bg-pattern']);
        }

        public function _getOTPVerify($id)
        {

            $usermobile =  \DB::table('user_otp')->select('mobile')->where(['user_id'=>$id])->get()->first();
           
            return view('dashboard.otp-verify', ['bodyClass' => 'authentication-bg authentication-bg-pattern','usermobile'=>$usermobile,'user_id'=>$id]);
        }

        public function _postOTPVerify(Request $request)
        {
            $validator = Validator::make($request->all(),
            [
                'otp1' => 'required',
                'otp2' => 'required',
                'otp3' => 'required',
                'otp4' => 'required',
            ],
                [
                    'otp1.required' => __('The OTP is required'),
                    'otp2.required' => __('The OTP is required'),
                    'otp3.required' => __('The OTP is required'),
                    'otp4.required' => __('The OTP is required')
                ]);

             if ($validator->fails()) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => 'The OTP is required'])->render()
                ]);
            }


        $credentials = [
            'otp' => request()->get('otp'),
        ];

        $otp = request()->get('otp1').request()->get('otp2').request()->get('otp3').request()->get('otp4');
         
        $userdata = \DB::table('user_otp')->where([['otp','=',$otp],[ 'otp_expirary','>=',time()]])->get();       
        
       
        if($userdata->count()>0)
         {
            \DB::table('user_otp')->where(['otp'=>  $otp])->delete();
            $redirect = get_referer(url('dashboard'));
           $usercred = \DB::table('users')->where(['id'=>request()->user])->get()->first();   
           $user = get_user_by_email($usercred->email);
           update_user_meta($user->id,'is_otp_verified',1);
           

         
          
           $user = Sentinel::login($user);
                   
             $userdata =     \DB::table('role_users')->select('*')->where([['user_id','=',get_current_user_id()]])->get()->first(); 
                
                $documentverify =  \DB::table('profile_verification_docs')->select('is_verified')->where(array('user_id'=>get_current_user_id()))->first();
                
        if(isset($userdata->role_id) && $userdata->role_id !=1)
        {
                    if(is_user_logged_in() and !empty($documentverify) and  !$documentverify->is_verified)
                    {
                         $redirect = get_referer(url('/verification'));
                    }
                    
                    if( is_user_logged_in() and  !isset($documentverify->is_verified))
                    {
                           $redirect = get_referer(url('/verification'));
                    }
        }
           
             if (Sentinel::check()) {  
                   /***** Check if ref  credits need to add  */
                    $this->calCulateCredits($user);
                        return $this->sendJson([
                        'status' => 1,
                        'message' => view('common.alert', ['type' => 'success', 'message' => __('Your Account is verified!')])->render(),
                        'redirect' =>$redirect
                    ]); 
             }else
             {
                        return $this->sendJson([
                        'status' => 0,
                        'message' => view('common.alert', ['type' => 'danger', 'message' => 'The OTP is not Valid'])->render()
                    ]); 

             }  
            
        }  
        else{
            return $this->sendJson([
                'status' => 0,
                'message' => view('common.alert', ['type' => 'danger', 'message' => 'The OTP is not Valid'])->render()
            ]); 


        }
       
            
        }
        
/**
 * Calculate Credit as per Refernce sign up 
 * @param user
 * @return  void
 * 
 **/ 
function calCulateCredits($user)
     {
          /****** Check if reffered user and assign credit accordingly */
        $ref = get_user_meta($user->id,'customer_ref_code','');
        if(!empty($ref))
        {  
                 $userrefid   = explode('_', $ref);
                 $total_credit_earning = get_user_meta($userrefid[1],'total_credit_earning');
                 $earning = get_option('signup_bonus', '');  
                 if(!empty($total_credit_earning))  
                 {
                    $earning  = $total_credit_earning +  $earning;     
                 }
                 $messagecontent  ='You have earned '.$earning.' credits as '.$user->first_name.' '.$user->last_name. ' just signed up by your reference';
                 $userdata = get_user_by_id($userrefid[1]);
                 Mail::send('referermail', array('maildata'=>$messagecontent,'user'=>$userdata), function($message)use($userdata) {
                 $message->to($userdata->email, $userdata->first_name.' '.$userdata->last_name)->subject
                  ( 'New Referal Earning');
                 $message->from(get_option('email_from_address', ''),get_option('email_from', ''));
                });            
           update_user_meta($userrefid[1],'total_credit_earning',$earning);
        } 
    }

        public function _postResetPassword(Request $request)
        {

            $validator = Validator::make($request->all(),
                [
                    'email' => 'required|exists:users,email',
                ],
                [
                    'email.required' => 'The email is required',
                    'email.exists' => 'The email does not exist',
                ]
            );

            if ($validator->fails()) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => $validator->errors()->first()])->render()
                ]);
            }

            $credentials = [
                'login' => request()->get('email'),
            ];

            $user = Sentinel::findByCredentials($credentials);
            if (is_null($user)) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => __('The email does not exist')])->render()
                ]);
            } else {
                $password = createPassword(32);
                $credentials = [
                    'password' => $password,
                ];

                $user = Sentinel::update($user, $credentials);

                if (!$user) {
                    return $this->sendJson([
                        'status' => 0,
                        'message' => view('common.alert', ['type' => 'danger', 'message' => __('Can not reset password for this account. Try again!')])->render()
                    ]);
                } else {
                    $subject = sprintf(__('[%s] You have changed the password'), get_option('site_name'));
                    $content = view('frontend.email.reset-password', ['user' => $user, 'password' => $password])->render();
                    $sent = send_mail(get_option('email_from_address', ''), get_option('email_from', ''), $user->email, $subject, $content);
                    if (!$sent) {
                        return $this->sendJson([
                            'status' => 0,
                            'message' => view('common.alert', ['type' => 'danger', 'message' => __('Can not send email.')])->render(),
                        ]);
                    }
                    return $this->sendJson([
                        'status' => 1,
                        'message' => view('common.alert', ['type' => 'success', 'message' => __('Successfully! Please check your email for a new password.')])->render(),
                        'redirect' => auth_url('login')
                    ]);
                }
            }
        }

        public function _getLogin()
        {
            return view('dashboard.login', ['bodyClass' => 'authentication-bg authentication-bg-pattern']);
        }

        public function _postLogin(Request $request)
        {
            $input = request()->only('email', 'password');
             $redirect = get_referer(url('dashboard'));
            if(isset($_POST['redirect'])):
             $redirect = get_referer(url(request()->redirect));
            endif;
           
           
            $validator = Validator::make($request->all(),
                [
                    'email' => 'required|exists:users,email',
                    'password' => 'required|min:6'
                ],
                [
                    'email.required' => __('The email is required'),
                    'email.exists' => __('The email does not exist'),
                    'password.required' => __('The password is required'),
                    'password.min' => __('The password has at least 6 characters')
                ]
            );
            if ($validator->fails()) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => $validator->errors()->first()])->render()
                ]);
            }
            try {

                   $data = \DB::table('users')->select('*')->join('role_users','role_users.user_id','=','users.id')->where([['email','=',$request->email]])->get()->first();
                
              
                
                if(!empty($data)  and isset($data->role_id) and get_user_meta($data->id,'is_otp_verified',0) == 0 and @$data->role_id !=1)
                {
                    
                    
                     try
                    {
        
                   
                    $sendotp = new \App\Controllers\TwilioController();
                    $sendotp->sendOTP(['user_id'=>$data->id,'mobile'=>$data->mobile]);  
                   
                   
                  
                        return $this->sendJson([
                            'status' => 1,
                            'message' => view('common.alert', ['type' => 'success', 'message' => 'OTP Sent to your registered mobile number '])->render(),
                            'redirect' => url('auth/otp-verify/'.$data->id)
                        ]);
                    
        
                     }catch(\Twilio\Exceptions\RestException $ex)
                     {
        
                          return $this->sendJson([
                            'status' => 0,  
                            'message' => view('common.alert', ['type' => 'danger', 'message' => $ex->getMessage()])->render()
                        ]);
        
                     }
                    
                }
                
                
                
                  Sentinel::authenticate($input, request()->has('remember'));
                
             $userdata =     \DB::table('role_users')->select('*')->where([['user_id','=',get_current_user_id()]])->get()->first(); 
                
                $documentverify =  \DB::table('profile_verification_docs')->select('is_verified')->where(array('user_id'=>get_current_user_id()))->first();
                
        if(isset($userdata->role_id) && $userdata->role_id !=1)
        {
                    if(is_user_logged_in() and !empty($documentverify) and  !$documentverify->is_verified)
                    {
                         $redirect = url('/verification');
                    }
                    
                    if( is_user_logged_in() and  !isset($documentverify->is_verified))
                    {
                           $redirect = url('/verification');
                    }
        }
            } catch (NotActivatedException $e) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => __('User is not activated')])->render()
                ]);

            } catch (ThrottlingException $e) {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => __('Your account has been suspended due to 5 failed attempts. Try again after 15 minutes.')])->render()
                ]);

            }
            if (Sentinel::check()) {
                return $this->sendJson([
                    'status' => 1,
                    'message' => view('common.alert', ['type' => 'success', 'message' => __('Logged in successfully. Redirecting ...')])->render(),
                    'redirect' => $redirect
                ]);
            } else {
                return $this->sendJson([
                    'status' => 0,
                    'message' => view('common.alert', ['type' => 'danger', 'message' => __('The email or password is incorrect')])->render()
                ]);
            }
        }

        public function _postLogout(Request $request)
        {
            $redirect_url = request()->get('redirect_url');

            Sentinel::logout();

            if (empty($redirect_url)) {
                $redirect_url = url('auth/login');
            }
            return $this->sendJson([
                'status' => 1,
                'title' => 'System Alert',
                'message' => __('Successfully Logged out'),
                'redirect' => $redirect_url
            ]);
        }

        public function _getLogout(Request $request)
        {
            $redirect_url = request()->get('redirect_url');

            Sentinel::logout();

            if (empty($redirect_url)) {
                $redirect_url = url('auth/login');
            }
            return redirect($redirect_url);
        }
    }
