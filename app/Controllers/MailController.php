<?php
/**
 * Created by PhpStorm.
 * User: Jream
 * Date: 12/9/2019
 * Time: 10:47 PM
 */

namespace App\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use DB;

class MailController extends Controller
{
    private $mail;

    public function __construct()
    {
        $this->mail = new PHPMailer();
        $this->mail->IsSMTP();
        $this->mail->Host = get_option('smtp_host');
        $this->mail->SMTPAuth = true;
        $this->mail->Username = get_option('smtp_username');
        $this->mail->Password = get_option('smtp_password');
        $this->mail->SMTPSecure = get_option('type_encrytion');
        $this->mail->Port = get_option('smtp_port');

        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

    }

    public function setEmailFrom($from, $from_label = '')
    {
        $this->mail->setFrom($from, $from_label);
    }

    public function setEmailTo($to)
    {
        $this->mail->AddAddress($to);
    }

    public function setReplyTo($reply_to)
    {
        $this->mail->addReplyTo($reply_to);
    }

    public function sendMail($subject, $body)
    {
        $this->mail->Subject = $subject;
        $this->mail->Body = $body;
        $this->mail->WordWrap = 50;
        $this->mail->IsHTML(true);
        $this->mail->CharSet = 'UTF-8';
        return $this->mail->send();
    }

    public function _emailChecker(Request $request)
    {
        return view('dashboard.screens.administrator.email-checker');
    }

    public function sendconfirmationmail()
    {
        //dd('test');
        $user_data= get_current_user_data();

        $user_id = get_current_user_id();
        $user_name = get_username($user_id);
        $user_email = $user_data->email;
        //dd($user_email);
        $user_name = $user_data->first_name;
        //$email_to = "siddharth@virtualemployee.com";
        $email_to = $user_email;
        //dd($email_to);
        $body = '<body style="margin:0; padding:0;border-collapse:collapse; max-width:600px; width:100%; margin:0 auto;     background-color: #fff;
box-shadow: 0px 0px 10px rgba(0,0,0,0.2);
padding-top: 10px; "><table cellpadding="0" border="0" cellspacing="0" style="max-width:580px; width:100%; background-color:#eee; margin:0 auto; border-collapse:collapse;">
<tbody style="">
<tr style="background-color:#fff; ">
<td >
<p style="font-size: 14px;
color: #187fff;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto;">BOOK A MUSIC STUDIO</p>
</td>
<td >
<p style="font-size: 14px;
color: #000;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto;
text-align:right; ">STUDIOS FOR MUSIC <br>SOUND BOX STUDIOS</p>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<p style="font-size: 22px;
color: #187fff;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto;
font-weight:600;
padding-bottom:20px; ">Hi '.$user_name.'</p>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<p style="font-size: 28px;
color: #1a0000;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto;
font-weight:600;">Your account has been verified. You are now free to make a booking 24/7 via our website.</p>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<p style="font-size: 16px;
color: #1a0000;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto; ">We look forward to having you use our creative space when it suits you best. If you have any questions, please do not hesitate to get in touch.</p>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<p style="font-size: 16px;
color: #1a0000;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto; ">Team Soundbox</p>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<p style="font-size: 16px;
color: #1a0000;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto; ">0431 736 798</p>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<a href=""><p style="font-size: 14px;
color: #2c8aff;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto;
padding-bottom:50px;">hello@soundboxstudios.com.au</p></a>
</td>
</tr>
<tr style="background-color:#fff; ">
<td colspan="2">
<p style="font-size: 14px;
color: #1a0000;
padding: 0px 42px;
margin: 10px 0px;
font-family:Roboto;
padding-bottom:50px; display: flex;
padding-top:50px;
align-items: center;
border-top:1px solid #ccc;
text-align:center;
justify-content: center;">  <img src="images/copyright_PNG59.png" style="width:15px; margin-right:5px;  "/>2022 - studio for music - Soundbox Studios | Book a Music Studio</p>
</td>
</tr>
</tbody>
</table>
</body>';

//die("stop");
       // $email_content = "Your document is sucessfully verified";
       $email_content = $body;
       //dd($email_content);
       //$password="12345";
       //$email_content = view('frontend.document.email')->render();
       //$content = view('frontend.email.welcome-user',compact('user_data','password'))->render();
       //dd(view('frontend.document.email'));
        $email_subject = "Document verification";

        $from = get_option('email_from_address');
        $from_name = get_option('email_from');

      //dd("test");

        if (!is_email($email_to)) {
            return $this->sendJson([
                'status' => 0,
                'message' => __('The email address is incorrect')
            ]);
        }
        

        $this->setEmailFrom($from, $from_name);
        $this->setEmailTo($email_to);
        $message ='';

        $sent = $this->sendMail($email_subject, $email_content);
        if ($sent) 
        {
            //dd("test");
            $documentverify =  \DB::table('profile_verification_docs')->select('is_verified')->where(array('user_id'=>get_current_user_id()))->first();
            //$verified_user =  \DB::table('profile_verification_docs')->where('email', '=', Input::get('email'))->first();
            
            
             if ($documentverify === null) 
            {
             
             $user_id =get_current_user_id();
                 \DB::table('profile_verification_docs')->insert(['user_id'=>$user_id,'is_verified'=>1]);
               
             }
           
            $verify=$documentverify->is_verified;
            
            if($verify == "0")
            {
                //dd('need to update');
                DB::table('profile_verification_docs')->where(array('user_id'=>get_current_user_id()))->update(['is_verified'=>1]);
                //dd('updated userid');
                
            }
            //dd("test");
            
            
           // dd($documentverify->is_verified);
            $message = 'Sent email. Please check your mailbox';
        } 
        else {
            $message = 'Sent emailffailed. Please try again'.$this->mail->ErrorInfo;
           
        }
        return view("frontend.document.welcome",compact('message'));
    }
    
}
